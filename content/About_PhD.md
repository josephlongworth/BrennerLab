+++
title = "About PhD"
description = "About obtaining a phd in the Group"
+++

# About obtaining a PhD in the Group

> Within the running [NextImmune2](https://www.lih.lu/en/nextimmune2/) PhD program in the field of chronic inflammation, the group is seeking excellent and highly motivated candidates holding a Master’s degree (MSc or equivalent). The group is always interested in motivated, eager and keen scientists with a strong scientific background.

Interested in joining the team? Please contact us on EMI@LIH.lu
