+++
title = "about"
description = "Who we are"
+++


# Experimental and Molecular Immunology

{{< youtube id="F7CfcMuxZM8" image="/BrennerLab/img/Profile_Pages/Joseph_Longworth.jpg" >}}


## Mission 
The Experimental and Molecular Immunology laboratory is an international research group within LIH’s Department of Infection and Immunity. Our research group focuses on the control of inflammation, autoimmune responses, and cancer by metabolic adaptation of immune cells.

## Vision 
The Brenner laboratory is strongly dedicated to cutting-edge research. As such, our group combines metabolic, molecular, cellular, and physiological approaches to unravel new ways to control immunity. Our vision is to develop new concepts for personalized medicine to mitigate inflammatory diseases through a mechanism-centered approach. As such, our group promotes a concept in which diseases are not treated based on symptoms, but on the mechanistic and individual cause of the disease.

## Activities 

The immune system is crucial for a health body function and protects us from severe infection. However, dysregulated immunity can cause inflammation, autoimmune diseases and cancer. Especially, the control of immune cell metabolism as emerged as a powerful way to regulate immunity. The Brenner laboratory investigates the metabolic regulation of the immune system and how this ensures a coordinated immune response and homeostasis. We seek to define the molecular, metabolic and cellular processes of inflammation and integrate in vitro with in vivo studies to gain a comprehensive picture of inflammation and cancer. A key aspect and focus of all our projects is the identification of novel metabolic checkpoints that influence the regulation of the immune system. One of these key circuits is the regulation of redox metabolism and reactive oxygen species (ROS) in immune cells. We investigate physiological consequences of ROS accumulation and their impact on immune cell function in health and disease. 
