+++
title = "Media"
description = "Latest hightlights from the Group Media"
+++

# Media

## 2022

- **Trierischer Volksfreund**, 22.05.2022 - [Letzte Corona-Regeln werden verlängert](https://www.volksfreund.de/region/rheinland-pfalz/rlp-letzte-corona-regeln-werden-verlaengert-prognose-impfung-mutanten_aid-70336253)
- **Linked In** – Luxembourg Institute of Health’s Post, 04.04.2022 - [Insights in the regulation of anti-viral immunity](https://www.linkedin.com/feed/update/urn:li:activity:6917052309667495936)
- **LIH.Twitter**, 04.04.2022 - [B or not to B: Insights in the regulation of anti-viral immunity](https://twitter.com/LIH_Luxembourg/status/1510995607103569924)
- **LIH.lu**, 04.04.2022 - [B or not to B: Insights in the regulation of anti-viralimmunity](https://www.lih.lu/en/b-or-not-to-b-insights-in-the-regulation-of-anti-viral-immunity/)
- **Science.lu**, 04.04.2022 - [Immunabwehr und Antikörper - Einblicke in die Regulierung der antiviralen Immunität](https://science.lu/de/immunabwehr-und-antikoerper/einblicke-die-regulierung-der-antiviralen-immunitaet)
- **Press release**, 04.2022 - [Franchina et al Nature Communications](https://www.nature.com/articles/s41467-022-29426-x?proof=tConcernant)
- **Tageblatt**, 11.04.2022 - [Omikron BA.2: Infektiös wie Masern?](https://www.tageblatt.lu/headlines/ba-2-variante-so-infektioes-wie-masern-luxemburger-forscher-klaert-ueber-die-neue-omikron-variante-auf/)
- **Trierischer Volksfreund**, 31.03.2022 - [Experte: Jungen Geboosterten steht unbeschwerter Sommer bevor](https://www.volksfreund.de/app/consent/?ref=https://www.volksfreund.de/region/rheinland-pfalz/corona-experte-erwartet-unbeschwerten-sommer-fuer-junge-geboosterte_aid-67673279)
- **Trierischer Volksfreund**, 29.03.2022 - [Ist trotz weiter steigender Corona-Zahlen ein Ende fast aller Massnahmen vertretbar?](https://e-paper.volksfreund.de/webreader-v3/index.html#/984135/2-3)
- **Trierischer Volksfreund**, 28.03.2022 - [Corona-Massnahmen im Land könnten am Wochenende enden](https://www.volksfreund.de/app/consent/?ref=https://www.volksfreund.de/nachrichten/politik/wie-geht-es-bei-den-corona-beschraenkungen-weiter_aid-67575689)
- **Trierischer Volksfreund**, 25.02.2022 - [Luxemburger Wissenschaftler: Das kann Deutschland von anderen Ländern bei Corona-Lockerungen lernen](https://www.volksfreund.de/app/consent/?ref=https://www.volksfreund.de/region/corona-lockerungen-das-kann-deutschland-von-daenemark-lernen_aid-66378973)
- **MinSanté_Lunchtalk**, 10.02.2022 - [Médicaments et nouveau vaccin contre le COVID-19, Dr Chioti & Prof Dr Brenner webex meeting](https://www.youtube.com/watch?v=jQwum4-kFDI&t=1s)
- **Luxemburger Wort**, 03.02.2022 - [Mehr testen für mehr Sicherheit-Schnelltests erkennen Omikron schlechter als andere Mutanten-effizient bleiben sie dennoch]()
- **Tageblatt**, 02.02.2022 - [Wie gefährlich ist die Omikron-Variante?Pandemieschäden: Vier Experten klären auf](https://www.tageblatt.lu/headlines/wie-ungefaehrlich-ist-omikron-wirklich-luxemburger-experten-klaeren-auf/)

## 2021

- **Tageblatt**, 14.12.2021 - [Unsere Gesellschaft ist noch nicht bereit - Interview - "Ein Immunologe über die Ankunft der Omicon-Variante in Luxemburg"](https://www.tageblatt.lu/headlines/luxemburger-forscher-ueber-omikron-unsere-gesellschaft-ist-noch-nicht-bereit/)
- **Science.lu**, 01.09.2021 - [Der bessere Impfschutz - Was schützt besser vor Covid-19: Impfung oder überstandene Erkrankung?](https://www.volksfreund.de/region/zutritt-nur-noch-fuer-geimpfte-diskussion-ueber-2-g-regel_aid-62463297)
- **Trierischen Volksfreund**, 30.08.2021 - [2G-Regel oder 3G-Regel in Rheinland-Pfalz? Experten warnen vor Impfpflicht durch die Hintertür]
- **Trierischen Volksfreund**, 20.08.2021 - [Corona Region Trier: Wie Impfungen wirken, warum Inzidenzen steigen - Zahlen aus der Region zeigen: Impfungen wirken – trotzdem beginnt vierte Corona-Welle](https://www.volksfreund.de/region/corona-region-trier-wie-impfungen-wirken-warum-inzidenzen-steigen_aid-62291261)
- **Trierischen Volksfreund**, 30.07.2021 - [Seniorenheim Morbach: Zweu Corona-Infektionen bei Bewohnern - Zwei Seniorenheime - Bewohner in Morbach mit Coronavirus infiziert (Update)](https://www.volksfreund.de/region/mosel-wittlich-hunsrueck/seniorenheim-morbach-zwei-corona-infektionen-bei-bewohnern_aid-61864693)
- **Trierischen Volksfreund**, 28.06.2021 - [Corona-Impfung: Rheinland-Pfalz will Sonder-Aktionen starten - Mehr Tempo beim Corona-Schutz](https://www.volksfreund.de/region/corona-region-trier-wie-impfungen-wirken-warum-inzidenzen-steigen_aid-62291261)
- **Trierischen Volksfreund**, 25.06.2021 - [Rheinland-Pfalz plant Sonderimpfaktion - Fragen un Antworten zum Impfen - Immunologe: "Jeder der sich nicht impfen lässt wird sich infizieren" - Land plant Sonderimpfaktion](https://www.volksfreund.de/region/rheinland-pfalz-plant-sonderimpfaktion-fragen-und-antworten-zum-impfen_aid-60183019)
- **Trierischen Volksfreund**, 17.06.2021 - [Impfung gegen Corona bei Kindern: Eltern müssen selbst entscheiden Kinder gegen Corona impfen, ja oder nein?](https://www.volksfreund.de/region/impfung-gegen-corona-bei-kindern-eltern-muessen-selbst-entscheiden_aid-59689469)
- **Luxemburger Wort**, 19.05.2021 - [Les vaccins, un marché marginal devenue gigantesque](https://www.wort.lu/de/business/les-vaccins-un-marche-marginal-devenu-gigantesque-60a528d7de135b92362d8828)
- **Trierischen Volksfreund**, 18.05.2021 - [Wie weit schützt die Impfung - Geimpfte haben geringeres Risiko an Corona zu sterben](https://www.volksfreund.de/nachrichten/politik/wie-weit-schuetz-die-impfung_aid-58209937)
- **Trierischen Volksfreund**, 14.05.2021 - [Seniorenhaus Prüm: 15 weitere Ansteckungen mit Sars-CoV-2 - auch Geimpfte betroffen](https://www.volksfreund.de/region/bitburg-pruem/seniorenhaus-pruem-23-bewohner-und-pflegekraefte-mit-corona-infiziert_aid-58122283)
- **Trierischen Volksfreund**, 11.05.2021 - [Experte: "Immun heisst nicht, dass man nicht mehr ansteckend ist" - Warum auch Geimpte sich noch infizieren können](https://www.volksfreund.de/region/warum-auch-geimpfte-sich-noch-mit-corona-anstecken-koennen_aid-58083213)
- **Trierischen Volksfreund**, 07.05.2021 - [Wieso Geimpfte in Luxemburg (noch) nicht mehr Rechte erhalten](https://www.volksfreund.de/region/wieso-geimpfte-in-luxemburg-noch-nicht-mehr-rechte-erhalten_aid-57980825)
- **Trierischen Volksfreund**, 07.05.2021 - [Was beim Muttertagsbesuch zu beachten ist](https://www.volksfreund.de/region/fragen-und-antworten-was-beim-muttertagsbesuch-zu-beachten-ist_aid-57980475)
- **Trierischen Volksfreund**, 07.05.2021 - [Behauptungen rund um die Corona-Impfung: Was ist wahr, was gelogen?](https://www.volksfreund.de/magazin/gesundheit/corona-impfung-faktencheck-geruechte-zu-fruchtbarkeit-dna-uebertragung_aid-57963429)
- **Trierischen Volksfreund**, 15.04.2021 - [Lockerungen für Geimpfte und Genesene](https://www.volksfreund.de/region/corona-lockerungen-fuer-vollstaendig-geimpfte-und-genesene_aid-57371437)
- **RND Redaktionsnetzwerk Deutschland**, 17.03.2021 - [Nutzen von Astrazeneca „massiv überwiegend“: Das sagen Corona-Experten zur EMA-Entscheidung](https://www.rnd.de/gesundheit/astrazeneca-das-sagen-corona-experten-zur-ema-entscheidung-2LSFHDERNNGALJJLAGQURHCNPY.html)
- **Trierischen Volksfreund**, 15.03.2021 - [Keine Impfungen mit AstraZeneca: So reagieren die Impfzentren in der Region](https://www.volksfreund.de/region/rheinland-pfalz-stoppt-12000-impftermine-mit-astrazeneca_aid-56815203)
- **Trierischen Volksfreund**, 11.03.2021 - [Der grosse Hintergrund: Können Schnelltests helfen, die Corona-Pandemie zu bekämpfen](http://www.volksfreund.de/region/faktencheck-koennen-corona-schnelltests-und-die-pandemie-stoppen_aid-56722853)
- **Trierischen Volksfreund** , 12.03.2021 - So funktionieren Schnelltests
- **Trierischen Volksfreund** , 12.03.2021 - [Das Impfen hat kaum Nebenwirkungen](http://www.volksfreund.de/region/immunologe-dirk-brenner-im-interview-impfen-hat-kaum-nebenwirkungen_aid-56749787)
- **Luxemburger Wort**, 03.03.2021 - [Schnelltests für Laien: Selbst ist der Patient - Antigene Selbstest](http://www.wort.lu/de/lokales/schnelltests-fuer-laien-selbst-ist-der-patient-603e703ade135b9236b8f91a)
- **L**'**Essentiel**, 24.02.2021 - [Pourquoi le Covid est parfois bénin, parfois grave](http://www.lessentiel.lu/fr/corona/story/pourquoi-le-covid-est-parfois-benin-parfois-grave-13893121)
- **Radio 100,7**, 04.01.2021 - [Covid-19-Vaccin eenzeg Léisung, mee zousätzlech Mesurë sinn néideg](https://100komma7.lu/program/episode/330573/202101040822-202101040825)
- **RTL Télé** - 31.01.2021 - [Pisa de Wëssensmagazin-Am Replay : Zaldoten am Blutt](https://www.rtl.lu/tele/pisa-de-wessensmagazin/a/1663282.html)

## 2020

- **Revue**, 03.06.2020 - [Ein Balanceakt](https://issuu.com/revue26/docs/revue_2020-23)
- **Le Quotidien**, 31.05.2020 - [La nutrition, clé anticancer, selon des chercheurs du Luxembourg](https://lequotidien.lu/politique-societe/la-nutrition-cle-anticancer-selon-des-chercheurs-du-luxembourg/)
- **Luxemburger Wort**, 06.05.2020 - [Autoimmunität und Krebs: Ernährung als Schlüssel](https://www.wort.lu/de/panorama/autoimmunitaet-und-krebs-ernaehrung-als-schluessel-5eb2cd6dda2cc1784e35d2d9)
- **Trierischer Volksfreund**, 06.05.2020 - [kampf gegen krebs luxemburger forscher praesentieren bahnbrechende studie_aid](https://www.volksfreund.de/nachrichten/wissenschaft/kampf-gegen-krebs-luxemburger-forscher-praesentieren-bahnbrechende-studie_aid-50397549)
- **Radio 100,7**, 28.04.2020 - [Immunitéit géint den neie Coronavirus](https://www.100komma7.lu/article/aktualiteit/immuniteit-geint-den-neie-coronavirus)
- **Science.lu**, 28.04.2020 - [Lutter contre les maladies auto-immunes et le cancer grâce à un régime nutritionnel spécifique](https://www.science.lu/fr/decouverte-majeure-luxembourg/lutter-contre-les-maladies-auto-immunes-le-cancer-grace-un-regime-nutritionnel-specifique)

## 2018

- **Science.lu**, 01.10.2018 - [Das Thema des Medizin-Nobelpreises 2018 wird auch in Luxemburg erforscht](https://www.science.lu/de/forschung-und-patientenbehandlung/das-thema-des-medizin-nobelpreises-2018-wird-auch-luxemburg-erforscht)
- **Science.lu**, 06.07.2018 - [Dirk Brenner is looking for new treatment methods for inflammatory diseases](https://www.science.lu/de/attract/dirk-brenner-sucht-neue-behandlungsmethoden-fuer-entzuendungskrankheiten)

## 2017

- **Trierischer Volksfreund**, 18.04.2017 - [Ein Schritt im Kampf gegen den Krebs](https://www.volksfreund.de/region/luxemburg/ein-schritt-im-kampf-gegen-den-krebs_aid-5616714)
- **Trierischer Volksfreund**, 18.04.2017 - [Spitzenforschung im Nachbarland: Luxemburg setzt verstärkt auf Wissenschaft](https://www.volksfreund.de/region/luxemburg/spitzenforschung-im-nachbarland-luxemburg-setzt-verstaerkt-auf-wissenschaft_aid-6604963)
- **Le Quotidien**, 30.04.2017 - [Cancer : des résultats « fascinants » et une piste prometteuse](http://www.lequotidien.lu/a-la-une/cancer-des-resultats-fascinants-et-une-piste-prometteuse/)