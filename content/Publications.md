+++
title = "Publications"
description = "Publication List for the Lab"
+++

# Selected Group Publications 


1) He, W., Henne, A., Lauterbach, M., Geißmar, E., Nikolka, F., Kho, C., Heinz, A., Dostert, C., Grusdat, M., Cordes, T., Härm, J., Goldmann, O., Ewen, A., Verschueren, C., Blay-Cadanet, J., Geffers, R., Garritsen, H., Kneiling, M., Holm, C.K., Metallo, C.M., Medina, E., Abdullah, Z., Latz, E., Brenner, D., and Hiller, K. (2022). [**Mesaconate is synthesized from itaconate and exerts immunomodulatory effects in macrophages.**](https://doi.org/10.1038/s42255-022-00565-1) **Nature Metabolism** 4, 524–533.

2) Kobayashi, T., and Brenner, D. (2022). [**A FAsT contribution: Adipocytes rewire their metabolism to acquire immune functions.**](https://doi.org/10.1016/j.cmet.2022.04.007) **Cell Metabolism** 34, 656–657.

3) Danileviciute, E., Zeng, N., Capelle, C.M., Paczia, N., Gillespie, M.A., Kurniawan, H., Benzarti, M., Merz, M.P., Coowar, D., Fritah, S., Vogt Weisenhorn, D.M., Gomez Giro, G., Grusdat, M., Baron, A., Guerin, C., Franchina, D.G., Léonard, C., Domingues, O., Delhalle, S., Wurst, W., Turner, J.D., Schwamborn, J.C., Meiser, J., Krüger, R., Ranish, J., Brenner, D., Linster, C.L., Balling, R., Ollert, M., and Hefeng, F.Q. (2022). [**PARK7/DJ-1 promotes pyruvate dehydrogenase activity and maintains Treg homeostasis during ageing.**](https://doi.org/10.1038/s42255-022-00576-y) **Nature Metabolism** 4, 589–607.

4) Franchina, D.G., Kurniawan, H., Grusdat, M., Binsfeld, C., Guerra, L., Bonetti, L., Soriano-Baguet, L., Ewen, A., Kobayashi, T., Farinelle, S., Minafra, A.R., Vandamme, N., Carpentier, A., Borgmann, F.K., Jäger, C., Chen, Y., Kleinewietfeld, M., Vasiliou, V., Mittelbronn, M., Hiller, K., Lang, P.A., and Brenner, D. (2022). [**Glutathione-dependent redox balance characterizes the distinct metabolic properties of follicular and marginal zone B cells.**](https://doi.org/10.1038/s41467-022-29426-x) **Nat Commun** 13, 1789.

5)  Kurniawan, H., Franchina, D.G., Guerra, L., Bonetti, L., - Baguet, L.S., Grusdat, M., Schlicker, L., Hunewald, O., Dostert, C., Merz, M.P., Binsfeld, C., Duncan, G.S., Farinelle, S., Nonnenmacher, Y., Haight, J., Das Gupta, D., Ewen, A., Taskesen, R., Halder, R., Chen, Y., Jäger, C., Ollert, M., Wilmes, P., Vasiliou, V., Harris, I.S., Knobbe-Thomsen, C.B., Turner, J.D., Mak, T.W., Lohoff, M., Meiser, J., Hiller, K., and Brenner, D. (2020). [**Glutathione Restricts Serine Metabolism to Preserve Regulatory T Cell Function.**](https://doi.org/10.1016/j.cmet.2020.03.004) **Cell Metabolism** 31, 920-936.e7.


6)  Cox, M.A., Duncan, G.S., Lin, G.H.Y., Steinberg, B.E., Yu, L.X., Brenner, D., Buckler, L.N., Elia, A.J., Wakeham, A.C., Nieman, B., Dominguez-Brauer, C., Elford, A.R., Gill, K.T., Kubli, S.P., Haight, J., Berger, T., Ohashi, P.S., Tracey, K.J., Olofsson, P.S., and Mak, T.W. (2019). [**Choline acetyltransferase–expressing T cells are required to control chronic viral infection.**](https://doi.org/10.1126/science.aau9072) **Science** 363, 639–644.

7)  Mak, T.W., Grusdat, M., Duncan, G.S., Dostert, C., Nonnenmacher, Y., Cox, M., Binsfeld, C., Hao, Z., Brüstle, A., Itsumi, M., Jäger, C., Chen, Y., Pinkenburg, O., Camara, B., Ollert, M., Bindslev-Jensen, C., Vasiliou, V., Gorrini, C., Lang, P.A., Lohoff, M., Harris, I.S., Hiller, K., and Brenner, D. (2017). [**Glutathione Primes T Cell Metabolism for Inflammation.**](https://doi.org/10.1016/j.immuni.2017.03.019) **Immunity** 46, 675–689.
*  Highlighted as a Preview:  [**Caught in the cROSsfire: GSH Controls T Cell Metabolic Reprogramming.**](https://doi.org/10.1016/j.immuni.2017.03.022) **Immunity** 46, 525–527.


8)  Brenner, D., Blaser, H., and Mak, T.W. (2015). [**Regulation of tumour necrosis factor signalling: live or let die.**](https://doi.org/10.1038/nri3834) **Nat Rev Immunol** 15, 362–374.


9)  Harris, I.S., Treloar, A.E., Inoue, S., Sasaki, M., Gorrini, C., Lee, K.C., Yung, K.Y., Brenner, D., Knobbe-Thomsen, C.B., Cox, M.A., Elia, A., Berger, T., Cescon, D.W., Adeoye, A., Brüstle, A., Molyneux, S.D., Mason, J.M., Li, W.Y., Yamamoto, K., Wakeham, A., Berman, H.K., Khokha, R., Done, S.J., Kavanagh, T.J., Lam, C.-W., and Mak, T.W. (2015). [**Glutathione and Thioredoxin Antioxidant Pathways Synergize to Drive Cancer Initiation and Progression.**](https://doi.org/10.1016/j.ccell.2014.11.019) **Cancer Cell** 27, 211–222.

10) Sasaki, M., Knobbe, C.B., Munger, J.C., Lind, E.F., Brenner, D., Brüstle, A., Harris, I.S., Holmes, R., Wakeham, A., Haight, J., You-Ten, A., Li, W.Y., Schalm, S., Su, S.M., Virtanen, C., Reifenberger, G., Ohashi, P.S., Barber, D.L., Figueroa, M.E., Melnick, A., Zúñiga-Pflücker, J.-C., and Mak, T.W. (2012). [**IDH1(R132H) mutation increases murine haematopoietic progenitors and alters epigenetics.**](https://doi.org/10.1038/nature11323) **Nature** 488, 656–659.


#### For a full list of the groups' publications please see [Google Scholar](https://scholar.google.com/citations?hl=de&user=-ucbI7oAAAAJ&view_op=list_works&sortby=pubdate).