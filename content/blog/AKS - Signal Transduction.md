+++
title = "STS Appointment"
date = "2021-02-01T12:00:00+01:00"
tags = ["News"]
categories = ["News"]
banner = "img/Blog_Images/STS_Logo.png"
authors = ["C Weis"]
+++
> Prof Dr Dirk Brenner elected co-coordinator of the Study Group - Signal Transduction within the German Society for Immunology (DGfI)

![STS Logo](
    /img/Blog_Images/STS_Logo.png#center)

[The Study Group - Signal Transduction (AKS - Signal Transduction)](https://dgfi.org/dgfi-en/study-groups/study-groups-overview/) counts 132 members in 2020 and provides an interdisciplinary forum for experts in the field of signal transduction processes in cells and organism.

The members unanimously elected Prof Dr Ingo Schmitz from the Ruhr-University in Bochum, Germany coordinator and Prof Dr Dirk Brenner co-coordinator. The appointment is from December 2020 to December 2023.

Prof Ingo Schmitz and Prof Dr Dirk Brenner represent the [DGfI](https://dgfi.org/dgfi-en/?noredirect=en_US) interests of the Signal Transduction Study Group and coordinate the information flow between both. They participate in the meetings of the DGfI Board & Advisory Board, are the contact persons for setting up new workgroups and organize the annual DGfI meetings of the workgroup spokespersons on:

- Signal Transduction in Immune Cells
- Lymphocyte Activation and Signal Transduction
- Immunometabolism
- Transcription and Epigenetics in the Immune System
- Signal Transduction in Immune System-related Diseases

Since 1996, the AKS – Signal Transduction actively organizes regular yearly congresses and teaching schools and coordinates since 1997 with workgroups within other associations; such as the German Society for Cell Biology - DGZ, Society for Biochemistry and Molecular Biology - GBM  and the German Society for Pharmacology - DGP. They founded together in 1998 the independent [Signal Transduction Society (STS)](https://sigtrans.de/).

An annual highlight of these interdisciplinary exchanges are the Joint “Signal Transduction: Receptors, Mediators and Genes” meetings in Weimar, organized by the STS committee.



