+++
title = "Anouk Ewen"
date = "2020-03-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","PhD Students"]
banner = "img/Profile_Pages/Anouk_Ewen.webp"
authors = ["J Longworth"]
+++

![Anouk_Ewen](/img/Profile_Pages/Anouk_Ewen.webp#center)


Anouk Ewen is a Luxembourgish PhD student in Prof. Brenner’s group ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg.

During her PhD studies, she mainly focuses on the innate immune response. She absolved her bachelor and master in biology at the Albert-Ludwigs-University of Freiburg, Germany, and worked already for her master project in the Experimental and Molecular Immunology group.

 
