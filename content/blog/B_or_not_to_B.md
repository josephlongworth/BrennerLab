+++
title = "B or not to B"
date = "2022-04-04T12:00:00+01:00"
tags = ["Paper","Publication"]
categories = ["Paper"]
banner = "img/Blog_Images/B_or_not_to_B/Graphical_Abstract.JPG"
authors = ["LIH Communications"]
+++

# B or not to B: Insights in the regulation of anti-viral immunity

> Researchers delve into the inner workings of the antibody immune response, finding key differences in the metabolism of two closely related immune cell subsets.

Protection against diseases such as viruses is a key part of our body’s immune response, where ‘antibodies’ are generated to seek out invading species and either neutralise or mark them for destruction.

![Figure 1](/img/Blog_Images/B_or_not_to_B/Cell_image.jpg#center) 

*“The importance of antibodies has been in the center of attention recently during the SARS-CoV2 pandemic and major efforts have been made to understand effective antibody responses”* explains Prof. Dirk Brenner, Deputy Head of the Department of Infection and Immunity (DII) at the LIH and Professor of Immunology and Genetics at the University of Luxembourg, when introducing his team’s latest study.

Antibody production is largely controlled by distinct types of ‘B cells’, special white blood cells whose purpose is to create the disease controlling antibodies.

In order to increase anti-viral immunity in the population, it is important to understand our bodies’ response to a disease causing virus, and not just the virus itself. A complete understanding of the immune response is therefore of paramount importance in developing future treatments, and this extends right down to the inner workings of individual cells. While, much is known about B cells that coordinate our antibody response, there are still big gaps in our understanding of what makes them tick on the inside. This study seeks to shed some light on that.

The purpose of this study has been to gain a better understanding of two different types of B cells, FoB and MZB cells. ‘Follicular’ B cells (FoB) tend to reside in small aggregations alongside other immune cells in places like the spleen and lymphnodes, while ‘marginal zone’ B cells (MZB) make up only about 5-10% of the B cells in a spleen where they can rapidly respond to blood-borne viral particles and invading bacteria.
The authors focused on the metabolic differences between both B cell subsets. That is, to see the ways in which they differ when creating and consuming energy, right up to the synthesis of new molecules and the breakdown and removal of others.

The way this has been achieved is through an elaborate game of ‘spot the difference’, targeting the production of a key chemical, glutathione, in each of the cell types and looking for changes in cell behavior. Normally, glutathione acts as an antioxidant in the cells, helping to maintain the delicate balance of reactions that regulate cell metabolism and keep things running as they should. By tipping the balance, researchers were able to look for differences that were unique to each cell type. 

When glutathione production was inhibited in the MZB cells of mice, a more significant impact was seen on normal cell development and maintenance, leading to an overall cell loss. FoB cells behaved in a more adaptive fashion, reprogramming their normal metabolic pathway so that they actually began to behave more like regular MZB cells. The downside for FoB cells however, was that this also led to an accumulation of defective mitochondria (the powerhouse of the cell), ultimately rendering them ineffective when exposed to viruses.

*“Our work highlights B cell-specific alterations that offer novel insights for the understanding of the role of glutathione in the regulation of B cells function and defects upon viral infections.”* summarises Dr Franchina, first author of the study from Prof. Brenner’s team at the DII. Ultimately, it is hoped that by continuing to build on our knowledge of these crucial gatekeepers, researchers can begin to translate the findings into novel treatment strategies for viral diseases such as SARS-CoV2, with tangible patient outcomes in the future.

The study was published on 4 April 2022 in Nature Communications, a multidisciplinary journal of the renowned Nature Research group, under the full title [“Glutathione-dependent redox balance characterizes the distinct metabolic properties of follicular and marginal zone B cells” (DOI: 10.1038/s41467-022-29426-x).](https://www.nature.com/articles/s41467-022-29426-x)

**Funding and collaborations**

*This study was supported by the FNR’s INTER-CORE [(C18/BM/12691266) program]. The work was also partially supported through intramural funding of LIH through the Ministry of Higher Education and Research (MESR) of Luxembourg.*


**About the Luxembourg Institute of Health (LIH)**

*The Luxembourg Institute of Health (LIH) is a public biomedical research organization focused on precision health and invested in becoming a leading reference in Europe for the translation of scientific excellence into meaningful benefits for patients.*

*LIH places the patient at the heart of all its activities, driven by a collective obligation towards society to use knowledge and technology arising from research on patient derived data to have a direct impact on people’s health. Its dedicated teams of multidisciplinary researchers strive for excellence, generating relevant knowledge linked to immune related diseases and cancer.*

*The institute embraces collaborations, disruptive technology and process innovation as unique opportunities to improve the application of diagnostics and therapeutics with the long-term goal of preventing disease.*
