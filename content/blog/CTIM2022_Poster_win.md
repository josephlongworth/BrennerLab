+++
title = "Leticia Wins Poster Prize Cell Symposia Basel"
date = "2022-06-28T12:00:00+01:00"
description = "Conference"
tags = ["Conference"]
categories = ["Conference","Presentation"]
banner = "img/Blog_Images/CTIM2022/Leticia_Poster.jpg"
authors = ["J Longworth"]
+++

> Presenting her work in a poster entitled *Pyruvate dehydrogenase fuels a critical citrate pool that is essential for Th17 cell effector function* Leticia Soriano Baguet from our lab won the poster prize 🏆 ! Great job, congrats & proud lab group 👏👏.

![CTIM_poster](/img/Blog_Images/CTIM2022/Leticia_Poster.jpg)
![CTIM_poster2](/img/Blog_Images/CTIM2022/Leticia_Win_1.jpg)