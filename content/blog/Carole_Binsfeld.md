+++
title = "Carole Binsfeld"
date = "2015-03-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Carole_Binsfeld.jpg"
authors = ["J Longworth"]
+++

![Carole_Binsfeld](/img/Profile_Pages/Carole_Binsfeld.jpg#center)



Carole Binsfeld was the first laboratory technician of Prof Dr Dirk Brenner, ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg. She has a master in Medical Biology and did her studies at the universities of Innsbruck (Bachelor studies) and Lausanne (Master studies). She works at LIH since 2014 (first for one year in the group of Dr Annette Kühn) and joined the team of Prof Brenner in 2015. 

She is involved in many different projects of the team and has acquired experience in a considerable number of techniques in cell culture, molecular biology and animal experimentation and is responsible for the ordering and organization of the lab.

 
