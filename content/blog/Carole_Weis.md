+++
title = "Carole Weis"
date = "2020-01-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Carole_Weis.jpg"
authors = ["C Weis"]
+++

![Carole_Weis](/img/Profile_Pages/Carole_Weis.jpg#center)


Carole obtained in 2003 the Diplôme de Brevet de Technicien Supérieur, assistante de direction, BTS, BAC+2, Ecole de Commerce et de Gestion, Luxembourg.

Since October 2003, she works within LIH. During 12 years, Carole was administrative assistant of Prof Dr Claude P Muller, Institute of Immunology, National Public Health Laboratory, LIH (former Centre de Recherche Public de la Santé). Since 2015, she is administrative officer at the Department of Infection and Immunity and since 2020; she is also administrative officer of the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab, LCSB, University of Luxembourg.
