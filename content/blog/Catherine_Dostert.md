+++
title = "Catherine Dostert"
date = "2015-07-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Catherine_Dostert.jpg"
authors = ["J Longworth"]
+++

![Catherine_Dostert](/img/Profile_Pages/Catherine_Dostert.jpg#center)



Catherine joined LIH in 2015. She is a scientist in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg.

Catherine Dostert studied Molecular and Cellular Biology at the University Louis Pasteur in Strasbourg, France. She started her PhD in the laboratory of Nobel Prize laureate Prof. Jules Hoffmann in Strasbourg, working on the antiviral immune response in Drosophila. She joined the laboratory of Prof Jürg Tschopp as EMBO long-term fellow to work on the inflammasome. Later she moved to Luxembourg and joined the University of Luxembourg as a postdoctoral fellow.
