+++
title = "Charlène Verschueren"
date = "2020-06-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Charlène_Verschueren.jpg"
authors = ["J Longworth"]
+++

![Charlène_Verschueren](/img/Profile_Pages/Charlène_Verschueren.jpg#center)



Charlène obtained a "brevet de technicien supérieur en Biotechnologie" in 2007 and a "licence génétique, biologie moléculaire et culture cellulaire" in 2008 at the IUT Nancy-Brabois, France. 

She started as laboratory technician in the retrovirology lab at the former CRP-Santé in September 2008. In 2015, the retrovirology group joined the Department of Infection and Immunity at the LIH. In 2020, Charlène joined the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg headed by Prof Dr Dirk Brenner.
