+++
title = "Davide's Defence"
date = "2021-09-28T12:00:00+01:00"
tags = ["PhD Defence"]
categories = ["PhD Defence"]
banner = "img/banners/IMG_6047.JPG"
authors = ["LIH Communications"]
+++

# Davide's Defence

Davide Franchina, who performed his PhD project in the Experimental and Molecular Immunology group at the Department of Infection and Immunity, successfully graduated with a Doctorate in Biology from the University of Luxembourg on 28th September 2021.

His four-year PhD project, supervised by Prof Dirk Brenner, was titled *“A dangeROS liaison: the metabolism-dependent regulation of B lymphocytes”. He investigated how metabolic stress in the form of reactive oxygen species (ROS) interferes with B cell function. Davide was funded through an FNR AFR bilateral grant and was associated to the FNR PRIDE NextImmune* Doctoral Training Unit.

Davide’s defence committee was composed of Prof Stephanie Kreis (UL) acting as chair, Prof Karl Lang (University Hospital Essen, Germany), Prof Julia Jellusova (Technical University Munich, Germany), Prof Hans-Martin Jäck (Friedrich-Alexander-Universität Erlangen-Nürnberg, Germany) and Prof Dirk Brenner (LIH/UL).  

The committee was impressed by Davide’s knowledge of the current literature in his research field and related areas. They commended him for his excellent presentation skills and significant research findings. So far, Davide has published three first-author reviews (1, 2, 3) and two research articles as co-author (4, 5). A first-author manuscript with the main findings form the PhD is currently under revision.

Given his excellent performance, the committee nominated him for the “Excellent Thesis Award” of the University of Luxembourg. Davide was not only committed to research alone during the PhD. He acted as member of the NextImmune steering committee and was one of the two PhD representatives of the Doctoral Programme in Systems and Molecular Biomedicine at the University of Luxembourg. He also organised PhD social events with the support from LIH, to give PhD candidates more opportunities to exchange and socialise. A highlight during Davide’s PhD was the attendance of the 2018 Lindau Nobel Laureate Meeting dedicated to physiology and medicine, a unique inspiring experience for a young researcher.

Congratulations!

 

*NextImmune “Next Generation Immunoscience: Advanced Concepts for Deciphering Acute and Chronic Inflammation” addresses key research and innovation challenges associated with the initiation, the diagnosis and the treatment of immune-mediated diseases. It aims to bridge classical immunology and big-data analysis science in a sole doctoral research and training environment. The programme started in 2017 and runs for 6.5 years in collaboration with the Universities of Luxembourg and of Southern Denmark.

## Image gallery

{{< image-gallery gallery_dir="Davide_Defence" >}}
