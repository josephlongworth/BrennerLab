+++
title = "Davide Franchina"
date = "2017-01-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","Alumni","Student Alumni"]
banner = "img/Profile_Pages/Davide_Franchina.jpg"
authors = ["C Weis"]
+++

![Davide Franchina](/img/Profile_Pages/Davide_Franchina.jpg#center)



> PhD defense 09.2021 within the NextImmune PhD Training Programme entitled *"A dangeROS liaison: the metabolism-dependent regulation of B lymphocytes"*

During his Phd, Davide investigated how metabolic stress in the form of reactive oxygen species (ROS) interferes with B cell function.

Davide successfully graduated with a Doctorate in Biology from the University of Luxembourg on September 28, 2021. 

The committee was impressed by Davide's knowledge of the current literature in his research field and related areas. They commended him for his excellent presentation skills and significant research findings and nominated him for the “Excellent Thesis Award” of the University of Luxembourg.
