+++
title = "Diana Helena Rodrigues Da Costa"
date = "2000-00-00T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Diana_Helena_Rodrigues_Da_Costa.jpg"
authors = ["J Longworth"]
+++

![Diana Helena Rodrigues Da Costa](/img/Profile_Pages/Diana_Costa.jpg#center)

Diana joined the lab as a laboratory technician in 2021 helping in a wide range of lab tasks including regular genotyping.