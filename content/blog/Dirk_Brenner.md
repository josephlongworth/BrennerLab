+++
title = "Dirk Brenner"
date = "2015-01-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Dirk_Brenner.jpg"
authors = ["J Longworth"]
+++

![DirkBrenner](img/Profile_Pages/Dirk_Brenner.jpg#center)

Professor Dirk Brenner is an enthusiastic immunologist who connects molecular, metabolic principles with the regulation of inflammatory diseases and cancer. He has a long-standing track record of significant publications as first, as well as senior author, in the fields of immunology, metabolism and cancer. He and his lab are specialized in in vivo applications and have significantly contributed to our understanding of how metabolism controls immunity in the recent past.
    
Prof Brenner studied biochemistry at the Universities of Bonn (Germany), Witten/Herdecke (Germany), Stanford University (USA), and Harvard University (USA). He moved to Heidelberg for his PhD and his early postdoctoral studies. At the German Cancer Research Center (DKFZ, Germany) he was a member of the internationally renowned group of Prof Dr Peter H. Krammer, who pioneered research on cell death.

As an ‘Alexander von Humboldt’ fellow, he joined the laboratory of Prof Dr Tak W. Mak at the Ontario Cancer Institute in Toronto (Canada), which is one of the world’s leading groups in biomedical research famous for the discovery of T cell receptor, the first physiological description of CTLA-4 and for reversed genetics. In that time, he was awarded with four postdoctoral fellowships including the competitive ‘Alexander von Humboldt foundation’ (Germany) and the ‘Cancer Research Institute’ (USA).
    
He received multiple national and international awards, e.g. the BD Lecture Prize (Canada), the Annual Award of the Signal Transduction Society (Germany) and an FNR-ATTRACT Consolidator Fellowship (Luxembourg), which brought him to Luxembourg.

Prof Brenner heads the ‘Experimental & Molecular Immunology’ lab (EMI) at the Luxembourg Institute of Health (LIH) and is deputy chair of the Department of Infection and Immunity (DII). He has been a founding member of the institute’s ‘Scientific Steering Committee’ and is deputy-coordinator of the graduate program ‘NEXTIMMUNE’. 

Prof Brenner has been appointed ‘Full Professor of Immunology and Genetics’ and Principal Investigator of the Immunology & Genetics group, which is strategic position between the Luxembourg Center of Biomedicine (University of Luxembourg, LCSB) and the LIH. In addition, he is an Adjunct Professor of Allergology at the University of Southern Denmark. He also serves as co-coordinator of the study group 'Signal Transduction' within the German Society of Immunology (DGfI) and is a member of the editorial board of 'Current Biology in Biotechnology' (Elsevier).
