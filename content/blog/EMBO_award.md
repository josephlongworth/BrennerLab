+++
title = "EMBO postdoctoral fellowship"
date = "2022-06-29T12:00:00+01:00"
description = "Conference"
tags = ["Grant"]
categories = ["Grant"]
banner = "img/Blog_Images/EMBO_banner.jpg"
authors = ["J Longworth"]
+++

![Veronika_Horková](/img/Profile_Pages/Veronika_Horková.jpg#center)

Congratulations to Nika Horkova for being awarded with an EMBO Postdoctoral Fellowship 🏆. Great start in the lab and delighted that Nika joined us 
here at the Brenner Lab.
