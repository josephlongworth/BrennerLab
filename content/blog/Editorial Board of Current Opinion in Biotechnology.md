+++
title = "Editorial Appointment"
date = "2021-02-01T12:00:00+01:00"
tags = ["News"]
categories = ["News"]
banner = "img/Blog_Images/Current_Opitions_in_Biotechnology.jpg"
authors = ["C Weis"]
+++

# Prof. Dr Dirk Brenner joins the Editorial Board of Current Opinion in Biotechnology, Elsevier

> Following the recommendation of the Editors-in-Chief Prof Greg Stephanopoulos and Prof Jan van de Meer, the editorial manager of Current Opinion in Biotechnology invited Prof Dr Dirk Brenner to join the Editorial Board. The appointment is for 3 years.

![Current opinions in Biotechnology](/img/Blog_Images/Current_Opitions_in_Biotechnology.jpg#center)

Current Opinion in Biotechnology is divided into 6 major sections, which are reviewed once a year. Each section covers the latest advances and trends in biotechnology and provides the views of experts on recent literature.

The role of Prof Dr Dirk Brenner is to suggest names of high-quality Section Editors, provide feedback and suggestions on potential review topics and authors proposed by the Section Editors, give feedback on the journal's development, including ideas for potential changes to the scope of individual sections.
