+++
title = "Publication: Nature Metabolism"
date = "2022-06-07T12:00:00+01:00"
tags = ["Paper","Publication","News"]
categories = ["Paper","News"]
banner = "img/Blog_Images/Nat_Met_Cover.png"
authors = ["Dirk Brenner & Karsten Hiller"]
+++


# Effective against an overactive immune system
Mesaconic acid: International research team discovers endogenous anti-inflammatory substance 


> *A research team led by Prof. Karsten Hiller of the Braunschweig Integrated Centre of Systems Biology, BRICS, in collaboration with scientists at the Luxembourg Institute of Health has discovered an anti-inflammatory chemical substance that the body naturally produces: mesaconic acid. This molecule could be a candidate for further development as a drug for treating septic shock or autoimmune diseases such as psoriasis and inflammatory bowel disease (IBD) – but without the side effects typically produced by the currently available anti-inflammatory drugs.*

Prof. Karsten Hiller’s team has been doing research for many years on metabolic products that are involved in the human immune system. In 2013, for example, they discovered that immune cells in the blood and brain of mammals produce itaconic acid – a substance that had only ever been found before as a metabolic product in fungi. Itaconic acid is a natural antibiotic, meaning it fights off bacteria and inhibits inflammations.

As a result of this discovery, itaconic acid has been the subject of many studies which were also supported within the framework of a binational research project between Prof. Hiller and the research group around the immunologist Prof. Dirk Brenner from the Luxembourg Institute of Health and the University of Luxembourg. During those studies, the researchers discovered that another metabolic product always occurs together with itaconic acid: mesaconic acid. Mesaconic acid is a chemical compound that the body produces from itaconic acid. Hiller relates, “We were interested in whether mesaconic acid also has an influence on inflammatory responses.” In experiments with laboratory mice, the research team discovered that this is indeed the case: "Mesaconic acid can revert an immune system that is currently “overshooting” back to the normal state” explains Prof. Brenner.

When effects like this are discovered, scientists need to gain a precise understanding of the metabolic processes behind it. In their investigations, a research consortium of nine research groups from Braunschweig, Bonn, Luxembourg, La Jolla (USA) and Arhus (Denmark), found that mesaconic acid has an anti-inflammatory effect of similar strength to that of itaconic acid. “However, there is a serious difference,” says Dr. Wei He, a member of Hiller’s team and first author of the study. “Unlike itaconic acid, mesaconic acid does not block the enzyme succinate dehydrogenase. This enzyme plays a central role in cell metabolism.”

Succinate dehydrogenase (SDH) is part of the respiratory chain. If it is inhibited – by itaconic acid, for example – this has strongly negative effects on metabolism. Since mesaconic acid has anti-inflammatory effects of similar strength to itaconic acid’s, but does not block the enzyme SDH, it makes an especially interesting candidate for an active agent against autoimmune diseases. “We now investigate why mesaconic acid has a positive anti-inflammatory effect on the immune system. We have already submitted another joint research proposal to do this," explain Prof. Hiller and Prof. Brenner

Once the researchers have precise answers to this question, concrete pharmacological investigations into mesaconic acid can begin. 
> *“Mesaconic acid could be considered for developing into an API for treating diseases in which the immune system is overactive – septic shock, for example, and especially autoimmune diseases such as psoriasis or inflammatory bowel disease”, says Karsten Hiller. “Maybe even with fewer side effects than other drugs, given that it is a substance that the body produces itself, and which does not interfere with the central metabolic pathways in cells.”*

## BRICS: Understanding Health

At the Braunschweig Integrated Centre of Systems Biology, BRICS, we want to understand what health means. For this, we investigate the molecular relationships that keep biological systems in balance. At BRICS, scientists use cutting-edge methods to observe biological cells and precisely determine the states of highly complex metabolic networks in their entirety. Using computer-assisted methods in bioinformatics, they identify those factors that cause biological systems to become unbalanced and thus lead to diseases. BRICS then uses the knowledge gained in this way to develop innovative therapies.
BRICS is a joint research centre of the TU Braunschweig, the Helmholtz Centre for Infection Research, and the Leibniz Institute DSMZ – German Collection of Microorganisms and Cell Cultures GmbH.

[www.tu-braunschweig.de/brics](www.tu-braunschweig.de/brics)

About the Luxembourg Institute of Health: Research dedicated to life….


[He, W., Brenner & Hiller, K. (2022). Mesaconate is synthesized from itaconate and exerts immunomodulatory effects in macrophages, Nature Metabolism, DOI: 10.1038/s42255-022-00565-1](https://www.nature.com/articles/s42255-022-00565-1)
