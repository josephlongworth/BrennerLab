+++
title = "Elisa Husting"
date = "2000-00-00T12:00:00+01:00"
description = "Who we are"
tags = ["Profile","intern"]
categories = ["intern"]
banner = "img/Profile_Pages/Elisa_Husting.jpg"
authors = ["J Longworth"]
+++

![Elisa Husting](/img/Profile_Pages/Elisa_Husting.jpg#center)



In May we were happy to welcome to the group Elisa Husting who is doing an internship with Veronika Horková. A aspiring young reseracher Elisa will gain invaluable experience from the lab and be an asset for our research activities. 
