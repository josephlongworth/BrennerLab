+++
title = "Henry's Defence"
date = "2020-07-01T12:00:00+01:00"
tags = ["PhD Defence"]
categories = ["PhD Defence"]
banner = "img/banners/77870-PhD-defence-Henry-Kurniawan.jpg"
authors = ["LIH Communications"]
+++

Henry Kurniawan from the Experimental and Molecular Immunology research group at the Department of Infection and Immunity received the PhD title from the University of Southern Denmark following his defence on 1st July held in Luxembourg and transmitted via videoconferencing.

Henry carried out his PhD project “Glutathione restricts serine metabolism to preserve regulatory T cell function” under the joint supervision of Prof Dirk Brenner and Prof Markus Ollert and was financially supported by Prof Brenner’s FNR ATTRACT Consolidator grant.

The defence committee, composed of Prof Uffe Holmskov (University of Southern Denmark), Prof Andreas Krueger (Goethe Universität Frankfurt) and Prof Katharina Lahl (Technical University Denmark), acknowledged the outstanding results of Henry’s research work that were recently published in the journal Cell Metabolism.

Congratulations to Henry!

## Image gallery

{{< image-gallery gallery_dir="Henry_Defence" >}}