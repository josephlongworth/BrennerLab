+++
title = "Henry Kurniawan"
date = "2016-04-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","Alumni","Student Alumni"]
banner = "img/Profile_Pages/Henry_Kurniawan.jpg"
authors = ["C Weis"]
+++

![Henry_Kurniawan](/img/Profile_Pages/Henry_Kurniawan.jpg#center)



> PhD defense 07.2020 within the NextImmune PhD Training Programme
entitled *"Glutathione restricts serine metabolism to preserve regulator"*

Henry Kurniawan comes from Indonesia. He went to the Netherlands to pursue his higher education at Utrecht University. After finishing his master degree in Drug Innovation entitled “Stress buffer effect of oxytocin and T cell reconstitution in HIV therapy”, Henry came across Dirk Brenner’s group “Experimental and Molecular Immunology”, which was just newly formed at that time.

Due to the exciting project, Henry decided to come to Luxembourg to do his PhD. He was working on the role of antioxidant glutathione on regulatory T cell metabolism. In July 2020 he finished his PhD entitled "Glutathione restricts serine metabolism to preserve regulatory T cell function", followed by a 6M Postdoc in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg. 
