+++
title = "Publication: Cell Metabolism"
date = "2020-05-01T12:00:00+01:00"
tags = ["Paper","Publication"]
categories = ["Paper"]
banner = "img/Blog_Images/Cell_Metabolism/cover.png"
authors = ["H Kurniawan"]
+++

# Cell Metabolism Publication

The immune system is crucial for a health body function and protects us from severe infection. However, a dysregulated immunity can cause inflammation, autoimmune diseases and cancer. Especially, the control of immune cell metabolism as emerged as a powerful way to regulate immunity. The Brenner Lab is investigates the metabolic regulation of the immune system and how this ensures a coordinated immune response and homeostasis. We seek to define the molecular, metabolic and cellular processes of inflammation and integrate in vitro with in vivo studies to gain a comprehensive picture of inflammation and cancer. 

![Figure 1](/img/Blog_Images/Cell_Metabolism/Figure_1.png)

***Figure 1.** Schematic diagram of energy-generating metabolic pathways and their regulators. (Kurniawan, Soriano-Baguet, Brenner et al.)*

Metabolism at a glance: Investing metabolic circuits in immune cells 
It has become clear that the metabolic reprogramming that occurs in activated immune cells is essential for their proper functions leading to the creation of a new field of research termed “immunometabolism”. Recent studies have underlined the role of cellular metabolism in the generation and maintenance of various types of immune cells. In T cells for example, distinct subsets utilize different energetic and biosynthetic pathways to sustain their activities. Naive CD4+ Th cells use oxidative phosphorylation (OXPHOS) for energy generation when in the quiescent state. Upon activation by antigen, these cells proliferate and differentiate into CD4+ T helper (Th) effector cells (Teffs), such as Th1, Th2, and/or Th17 cells and switch from OXPHOS to a highly glycolytic form of metabolism. In contrast, upon activation, regulatory T cells (Tregs) employ glycolysis at a low rate and high lipid oxidation. This divergent use of metabolic pathways can influence cell fate in various ways. For instance, fatty acid oxidation (FAO) fosters the generation of Tregs while dampening the polarization of Teffs. Our lab therefore focus on investigating how cellular metabolism affects cellular functions in specific contexts and various inflammatory diseases. Our analyses are not only focused on T cells, but also include innate immune cells and B cells. A key goal and focus of all our projects is to identify the metabolic checkpoints that interfere with cellular activity in the context of abnormal immune homeostasis like inflammation and cancer.

Glutathione (GSH) a major antioxidants and regulator of metabolism in the immune system
One of the major focus areas of our lab is the control of reactive oxygen species (ROS) by glutathione (GSH) in immune cells and the physiological consequences of ROS accumulation. ROS are balanced by antioxidants, which generally include molecules that are stable enough to donate electrons and thus act as ROS scavengers. The most abundant intracellular antioxidant system is GSH, a tri-peptide of γ-glutamyl-L-cysteinyl-glycine. The enzyme γ-glutamyl cysteine ligase (GCL), which is composed of a regulatory subunit (Gclm) and a catalytic subunit (Gclc), ligates glutamate and cysteine to form a di-peptide in the rate-limiting step of the synthetic pathway. This di-peptide is then covalently linked to glycine by GSH synthase (GS) to generate GSH.

We investigate the function of GSH in immune cells by reversed genetic approaches in vivo and in vitro. Conditional deletion of Gclc, the rate limiting enzyme in GSH-synthesis, in immune cells impairs the production of GSH.
For example, our group has identified GSH as an important regulator of T cell metabolism. We could show that the absence of GSH in conventional T cell lead to the accumulating ROS, which shuts down Teff responses. This prevents the onset of autoimmunity, but at the same time increases the susceptibility to viral infections. High ROS levels impair metabolic reprogramming in T cells by hindering proper activation of mTOR, NFAT and Myc and the establishment of T effector functions (Mak and Grusdat et al., Immunity 2017). (Figure 2).


 ![Figure 2](/img/Blog_Images/Cell_Metabolism/Figure_2.png)

***Figure 2:** GSH is a metabolic regulator of conventional T cells*

Interestingly GSH exhibits subset specific functions, which we are currently investigating in detail. It seems that GSH acts as a rheostat to adjust cellular ROS concentrations to cell subset-specific threshold level that allows for productive signaling and metabolism. 
Our group has recently shown that Tregs contain significantly higher cellular GSH concentrations, which renders them more resistant to increasing ROS. Conditional deletion of Gclc in murine Tregs impairs their production of GSH. These mutant Tregs consequently display increased levels of reactive oxygen species (ROS) but no apparent defects in Treg homeostasis, stability or differentiation. However, Gclc-deficient Tregs do show increased proliferation and activation and the mutant mice develop spontaneous autoimmunity. Importantly, Gclc-deficient Tregs show elevations in both their synthesis and uptake of serine. Accordingly, dietary intervention in the form of feeding the mutant mice on serine/glycine-deficient chow prevents the onset of autoimmunity. In line with this finding, dampening of serine metabolism reinstated the suppressive function of Tregs, firmly linking serine metabolism to Treg function. (Kurniawan et al. Cell Metabolism 2020) (Figure 3).


![Figure 3](/img/Blog_Images/Cell_Metabolism/Figure_3.png)


***Figure 3:** GSH restrains serine metabolism to ensure Treg cell functionality*

Our findings concerning the role of GSH in Tregs were presented on the cover of Cell Metabolism’s May issue 2020. (Figure 4)
 

![Figure 4](/img/Blog_Images/Cell_Metabolism/Figure_4.png)

***Figure 4:** Cover of Cell Metabolism, May 2020*
