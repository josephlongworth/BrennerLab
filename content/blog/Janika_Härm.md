+++
title = "Janika Härm"
date = "2021-11-02T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","PhD Students"]
banner = "img/Profile_Pages/Janika_Härm.jpg"
authors = ["J Longworth"]
+++

![Janika_Härm](/img/Profile_Pages/Janika_Härm.jpg#center)



Janika Härm is PhD student in Prof. Brenner’s group ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg.

She absolved her bachelor and master in biology at the Technical University, Braunschweig, Germany.

 
