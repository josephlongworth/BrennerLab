+++
title = "Joseph Longworth"
date = "2021-08-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Joseph_Longworth.jpg"
authors = ["J Longworth"]
+++

![JosephLongworth](/img/Profile_Pages/Joseph_Longworth.jpg#center)


<a href="mailto:joseph.longworth@lih.lu" target="_blank" style="opacity: 1;">
<i class="fas fa-2x fa-envelope"></i></a>
<a href="https://www.linkedin.com/in/joseph-longworth-06042b32/" target="_blank" style="opacity: 1;">
<i class="fab fa-2x fa-linkedin"></i></a>
<a href="https://github.com/JosephLongworth" target="_blank" style="opacity: 1;">
<i class="fab fa-2x fa-github"></i></a>
<a href="https://twitter.com/65Joseph" target="_blank" style="opacity: 1;">
<i class="fab fa-2x fa-twitter"></i></a>
<a href="tel:+352 26 970 382" target="_blank" style="opacity: 1;">
<i class="fas fa-2x fa-phone"></i></a>

Joseph studied his undergraduate in Molecular and Cellular Biology at the University of Glasgow and subsequently received a PhD from the University of Sheffield in 2014 working on proteomics investigations in eukaryotic microalgae.

Post PhD, he has worked throughout the field of proteomics across a wide gambit of biological queries including general mass spectrometry-based proteomics, CHO gene translational prediction, ubiquitin topology characterization and array-based proteomics (RPPA, Antigen arrays and Kinase arrays).

Joseph joined the Experimental & Molecular Immunology lab in 2021 taking on the role of Data Scientist. He is responsible for supporting the group in various bioinformatics data analyses harnessing talents in biological data interpretation with R, Python and Shiny. In addition, he aids in proteomic analysis within the group and kinase analysis for the wider LIH.

