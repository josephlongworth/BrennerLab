+++
title = "LCSB Retreat"
date = "2022-06-01T12:00:00+01:00"
tags = ["LCSB", "Retreat", "Fun"]
categories = ["Fun"]
banner = "img/Blog_Images/LCSB_Retreat/IMG_20220601_164630.webp"
authors = ["J Longworth"]
+++

# LCSB Retreat 2022

![Train Chat](/img/Blog_Images/LCSB_Retreat/IMG_20220601_164630.webp#fullwidth)

Group members joined in the recent team building retreat for LCSB.

With a morning session dedicated to increased interaction within LCSB, group members were teamed up with two other members of the LCSB to discuss the forfront of their research but more importantly getting to know each other on a personal level forstering potential future collaboration. 

In the afternoon we went to the Fond-de-Gras where we partook in an exciting scavanger hunt challenge followed by a vintage train ride. We ended the day with a nice BBQ at Bei-der-Giedel restaurant in Fond-de-Gras.
