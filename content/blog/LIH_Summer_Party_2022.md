+++
title = "LIH Summer Party 2022"
date = "2022-07-15T12:00:00+01:00"
description = ""
tags = ["LIH", "Retreat", "Fun"]
categories = ["Fun"]
banner = "img/Blog_Images/LIH_Summer_Party_2022/Banner.jpg"
authors = ["J Longworth"]
+++

> The group was excited to attend this year's LIH Summer party.  With a FIESTA theme, there was much enjoyment of fancy dress, Mexican-themed food and plenty of sports. A fabulous time had by all whether stung by a wasp or not. :)

![LIH_Party_2022](/img/Blog_Images/LIH_Summer_Party_2022/Banner.jpg)

## Image Gallery

{{< image-gallery gallery_dir="LIH_summer_party" >}}
