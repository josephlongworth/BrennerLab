+++
title = "Lab Fun"
date = "2020-06-12T12:00:00+01:00"

tags = ["Fun"]
categories = ["Fun"]
banner = "img/banners/Funny lab 1.JPG"
authors = ["J Longworth"]
+++

# Lab Fun 

Not just work the lab likes to have a laugh. Here are some pictures and a demonstration of the photo cauresel feature. 


## Image gallery

{{< image-gallery gallery_dir="album" >}}


{{< carousel2 items="1" height="500" unit="px" duration="6000" >}}
