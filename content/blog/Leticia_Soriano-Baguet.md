+++
title = "Leticia Soriano-Baguet"
date = "2018-10-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","PhD Students"]
banner = "img/Profile_Pages/Leticia_Soriano-Baguet.jpg"
authors = ["L Soriano-Baguet"]
+++

![Leticia_Soriano-Baguet](/img/Profile_Pages/Leticia_Soriano-Baguet.jpg#center)

<a href="mailto:Leticia.sorianobaguet@lih.lu" target="_blank" style="opacity: 1;">
<i class="fas fa-2x fa-envelope"></i></a>
<a href="https://www.linkedin.com/in/leticia-soriano-baguet-372068206" target="_blank" style="opacity: 1;">
<i class="fab fa-2x fa-linkedin"></i></a>

Leticia Soriano Baguet comes from Barcelona (Spain). There, she obtained a Bachelor in Biomedical Sciences and a Master’s in Advanced Immunology specializing in Medical Immunology from the University of Barcelona (UB, Spain) and Autonomous University of Barcelona (UAB, Spain).

Leticia is a PhD student in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg since October 2018. Her project focuses on metabolic gatekeepers in T cells: decision-making and controlling inflammation in vivo.
