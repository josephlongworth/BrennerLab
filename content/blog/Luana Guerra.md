+++
title = "Luana Guerra"
date = "2016-01-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","Alumni","Student Alumni"]
banner = "img/Profile_Pages/Luana_Guerra.webp"
authors = ["J Longworth"]
+++

![Luana_Guerra](/img/Profile_Pages/Luana_Guerra.webp#center)

# Luana Guerra

> PhD defense 11.2021 within the NextImmune PhD Training Programme
 entitled *"The anitoxidant glutathione as a regulator of Natural Killer (NK) cell immunity"*

Luana Guerra was a PhD student in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg.

Her four-year PhD project focused on the unravelling glutathione as a key checkpoint for Natural Killer (NK) cells metabolism and function.

Luana successfully graduated with a Doctorate in Biology from the University of Luxembourg on the 22nd of November 2021.

The committee was impressed by the excellent presentation of the research work, the logical setup of the research and her profound immunological knowledge. The jury described Luana as an ambitious young researcher with an analytical and critical mindset.
