+++
title = "Luana's Defence"
date = "2021-11-22T12:00:00+01:00"
tags = ["PhD Defence"]
categories = ["PhD Defence"]
banner = "img/banners/45585-PhD-defence-Luana-Guerra.JPG"
authors = ["LIH Communications"]
+++



On 22nd November, Luana Guerra from the Experimental and Molecular Immunology research group at the Department of Infection and Immunity presented her thesis work “The anitoxidant glutathione as a regulator of Natural Killer (NK) cell immunity” during a hybrid PhD defence held on the Belval campus in Esch-sur-Alzette.

For her PhD studies, she was affiliated with the University of Luxembourg and supported by a grant from the FNR PRIDE-funded doctoral research and training programme NextImmune*. The four-year PhD project, unravelling glutathione as a key checkpoint for NK cell metabolism and function, was supervised by Prof Dirk Brenner.  

Luana’s defence committee was composed of Prof Michel Mittelbronn (LNS/LIH/UL) acting as chair, Dr Greta Guarda (Università della Svizzera italiana, Switzerland), Prof Carsten Watzl (Technical University of Dortmund, Germany), Dr Bassam Janji (LIH) and Prof Dirk Brenner (LIH/UL).

The committee was impressed by the excellent presentation of the research work, the logical setup of the research and her profound immunological knowledge. The jury described Luana as an ambitious young researcher with an analytical and critical mindset. To date, Luana is first author of a review article on the metabolic modulation of immunity and co-author of a research article published in Cell Metabolism. Two other manuscripts are currently submitted for peer review.

 

Congratulations to Luana!

 

*NextImmune “Next Generation Immunoscience: Advanced Concepts for Deciphering Acute and Chronic Inflammation” addresses key research and innovation challenges associated with the initiation, the diagnosis and the treatment of immune-mediated diseases. It aims to bridge classical immunology and big-data analysis science in a sole doctoral research and training environment. The programme started in 2017 and runs for 6.5 years in collaboration with the Universities of Luxembourg and of Southern Denmark.

## Image gallery

{{< image-gallery gallery_dir="Luana_Defence" >}}
