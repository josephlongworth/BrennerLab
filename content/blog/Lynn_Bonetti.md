+++
title = "Lynn Bonetti"
date = "2017-01-03T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile","Alumni","Student Alumni"]
banner = "img/Profile_Pages/Lynn_Bonetti.jpg"
authors = ["C Weis"]
+++

![Lynn_Bonetti](/img/Profile_Pages/Lynn_Bonetti.jpg#center)



> PhD defense 11.2021 within the NextImmune PhD Training Programme entitled *"The Th17 cell – IL-22 axis depends on glutathione upon intestinal inflammation"*

During her PhD Lynn investigated the influence of the antioxidant glutathione in Th17 cells. She was able to decipher a signalling pathway in Th17 cells that depends on glutathione and leads to the formation of the protective cytokine IL-22. Using the murine bacterial infection model Citrobacter rodentium, she could show that glutathione in T cells is essential for the production of IL-22 and this T cell-generated IL-22 is in turn important in maintaining the integrity of intestinal tissue to prevent bacterial spread.

Lynn successfully graduated with a Doctorate in Biology at the University of Luxembourg on November 29, 2021.​

Given her excellent performance, the committee nominated her for the "Excellent Thesis Award" of the University of Luxembourg.
