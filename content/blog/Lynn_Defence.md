+++
title = "Lynn's Defence"
date = "2021-11-29T12:00:00+01:00"
tags = ["PhD Defence"]
categories = ["PhD Defence"]
banner = "img/banners/41070-PhD-defence-Lynn-Bonetti-2.jpg"
authors = ["LIH Communications"]
+++



> Lynn Bonetti from the Experimental and Molecular Immunology group at the Department of Infection and Immunity defended her PhD thesis on 29th November 2021 in hybrid format at the Belval campus in Esch-sur-Alzette. She will receive the Doctorate in Biology from the University of Luxembourg.

Lynn performed her four-year PhD project under the supervision of Prof Dirk Brenner and within NextImmune*, an FNR PRIDE Doctoral Training Unit that fosters next generation immunoscience. Her PhD thesis is entitled “The Th17 cell – IL-22 axis depends on glutathione upon intestinal inflammation”. Lynn  investigated  the  influence  of  the  antioxidant  glutathione  in  Th17  cells. She was able to decipher a signalling pathway in Th17 cells that depends on glutathione and leads to the formation of the protective cytokine IL-22. Using the murine bacterial infection model Citrobacter rodentium, she could show that glutathione in T cells is essential for the production of IL-22 and this T cell-generated IL-22 is in turn important in maintaining the integrity of intestinal tissue to prevent bacterial spread.

Lynn’s defence committee was composed of Prof Michel Mittelbronn (LNS/UL/LIH) acting as chair, Prof Philipp Lang (University of Düsseldorf, Germany), Prof Burkhard Becher (University of Zürich, Switzerland), Prof Christoph Wilhelm (University of Bonn, Germany) and Prof Dirk Brenner (LIH/UL). The committee was highly impressed by the comprehensive and confident presentation as well as the relevance and outstanding quality of the research findings. Her dissertation exceeded the expectations of a PhD thesis by far.

Until now, Lynn has contributed to two publications as a co-author (1, 2) and one review as a shared first author. She designed the cover of the Cell Metabolism issue from May 2020. A manuscript with the main findings of her PhD is currently in preparation. Lynn is also very committed to science communication to the lay public, having actively participated in the Science Festival, the RTL PISA Wëssensmagazin and the DESCOM comic Lux:plorations Vol.1.

Given her excellent performance, the committee nominated her for the “Excellent Thesis Award” of the University of Luxembourg. The Excellent Thesis Award winners will be announced at the PhD diploma ceremony end of 2022.

Congratulations on this outstanding achievement!  

 

*NextImmune “Next Generation Immunoscience: Advanced Concepts for Deciphering Acute and Chronic Inflammation” addresses key research and innovation challenges associated with the initiation, the diagnosis and the treatment of immune-mediated diseases. It aims to bridge classical immunology and big-data analysis science in a sole doctoral research and training environment. The programme started in 2017 and runs for 6.5 years in collaboration with the Universities of Luxembourg and of Southern Denmark.

## Image gallery

{{< image-gallery gallery_dir="Lynn_Defence" >}}
