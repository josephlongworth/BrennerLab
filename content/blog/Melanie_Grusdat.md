+++
title = "Melanie Grusdat-Pozdeev,"
date = "2016-01-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Melanie_Grusdat.jpg"
authors = ["J Longworth"]
+++

![Melanie_Grusdat](/img/Profile_Pages/Melanie_Grusdat.jpg#center)



Melanie Grusdat studied classical Biology at the Heinrich- Heine- University in Düsseldorf, Germany. She started her PhD in the newly founded laboratory of Sofja Kovalevskaja Adward winner Prof Philipp A Lang in Düsseldorf. Her PhD work was focused on the influence of different transcription factors in T cells and their functionality during viral infections.

She joined in 2016 the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg as a postdoctoral fellow in Luxembourg. Her postdoctoral research was focused on the influence of oxidative stress on T cell metabolism.
