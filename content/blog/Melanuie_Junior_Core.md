+++
title = "New Grant - M. Grusdat-Pozdeev"
date = "2022-02-25T12:00:00+01:00"
tags = ["Grant", "LCSB"]
categories = ["Grant"]
banner = "img/Blog_Images/Melanie_Junior_Core/logo_avec_tagline_couleur.png"
authors = ["J Longworth"]
+++

# New Grant - Melanie Grusdat-Pozdeev Junior Core


> *"Identification of clinically relevant compounds for the enhancement of CD8 T cell metabolism and function aims to identify pharmaceutical compounds that increase the success rate of immunotherapy and to identify small molecules that promote cell responses Anticancer CD8 cells in the tumor microenvironment."*

{{< youtube id="zb1eOEoHYIE" image="/BrennerLab/img/Profile_Pages/Joseph_Longworth.jpg" >}}



