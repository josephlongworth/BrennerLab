+++
title = "Sophie Farinelle"
date = "2017-07-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Sophie_Farinelle.jpg"
authors = ["J Longworth"]
+++

![Sophie_Farinelle](/img/Profile_Pages/Sophie_Farinelle.jpg#center)



Sophie Farinelle has a master in biology and she is a laboratory technician with 25 years of research laboratory experience in three different labs: first in the laboratory of Virologie fondamentale et Immunologie of Prof. B. Rentier at Université de Liège, then in the laboratory of Histology of Prof. J.-L. Pasteels at Université Libre de Bruxelles and now, for 19 years, at the Luxembourg Institute of Health in Luxembourg.

At LIH, she was first involved in the vaccinology team of Prof. CP Muller and since 2017, she’s working in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg headed by Prof Dr Dirk Brenner. She has acquired a good panel of laboratory techniques that are required for daily work.

She can work rigorously in different safety level laboratories: BSL1, BSL2, BSL3 (and ASL3 for animal experiments) and one of her key-skills is the animal biology experiment (Felasa B certificate) for vaccinology and immunological studies. She is now in charge of mouse colonies management in SPF facility, with maintenance and genotyping of mouse genetically-modified strains.
