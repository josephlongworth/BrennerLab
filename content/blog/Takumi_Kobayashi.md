+++
title = "Takumi Kobayashi"
date = "2020-02-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Takumi_Kobayashi.jpg"
authors = ["J Longworth"]
+++

![Takumi_Kobayashi](/img/Profile_Pages/Takumi_Kobayashi.jpg#center)


Takumi graduated with a BSc (Hons) from the University of Tasmania. Under the tutelage of Prof. Gregory Woods, he studied the immune system of the Tasmanian devil to understand how Tasmanian devils succumb to the Tasmania devil facial tumor disease (DFTD) as part of his Honors year project. He moved to the University of Queensland Diamantina Institute (UQDI) and worked on the development of natural killer (NK) T cell-targeted immune adjuvant and 4-1BB immune checkpoint combination immunotherapy against B cell lymphoma under Dr. Stephen Mattarollo.

He later pursued a PhD in cancer immunology in the same laboratory and developed an interest in the importance of cytotoxic lymphocytes, such as natural killer cells and CD8+ T cells, and fundamental processes that govern the function of these populations how blood tumor microenvironments perturb NK cell metabolism, leading to NK cell functional impairment.  

Since 2020, Takumi works as scientist in the ‘Experimental & Molecular Immunology’ lab of LIH and the ’Immunology & Genetics’ lab at LCSB of the University of Luxembourg. ​He is trying to understand how cellular metabolism regulates dendritic cells (DC), an important immune cell population that bridges innate and adaptive immune response, to mount anti-tumor immune response.
