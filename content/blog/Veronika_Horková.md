+++
title = "Veronika Horková"
date = "2021-07-01T12:00:00+01:00"
description = "Who we are"
tags = ["Profile"]
categories = ["Profile"]
banner = "img/Profile_Pages/Veronika_Horková.jpg"
authors = ["J Longworth"]
+++

![Veronika_Horková](/img/Profile_Pages/Veronika_Horková.jpg#center)



Nika grew up in Slovakia, where she obtained her International Baccalaureate Diploma. She then moved to Prague where she studied at the Faculty of Science of the Charles University. She obtained her Bachelor’s degree in Molecular Biology and Biochemistry of Organisms, followed by Master and PhD degrees in Immunology.

During her PhD studies, she worked on her thesis in the Laboratory of Adaptive Immunity at the Institute of Molecular Genetics of the Czech Academy of Science. She studied the role of interaction of CD4/CD8 co-receptors with the LCK kinase during T-cell development and immune response.

She joined the Laboratory of Immunology & Genetics at LCSB of the University of Luxembourg in the summer 2021 and she is currently a Postdoctoral fellow at the Laboratory of Experimental & Molecular Immunology at LIH.
